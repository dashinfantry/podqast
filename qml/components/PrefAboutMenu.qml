import QtQuick 2.0
import Sailfish.Silica 1.0

PushUpMenu {
    property string thelink: "https://gitlab.com/cy8aer/podqast/wikis/the-manual"

    MenuItem {
        text: qsTr("Settings")
        onClicked: pageStack.push(Qt.resolvedUrl("../pages/Settings.qml"))
    }
    MenuItem {
        text: qsTr("About")
        onClicked: pageStack.push(Qt.resolvedUrl("../pages/About.qml"))
    }
    MenuItem {
        text: qsTr("Help")
        onClicked: Qt.openUrlExternally(thelink)
    }
}
