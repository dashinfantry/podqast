<p>
<h3>Autores</h3>
  <a href="https://talk.maemo.org/member.php?u=33544">Cy8aer</a> (código)<br>
  <a href="https://talk.maemo.org/member.php?u=31168">BluesLee</a> (pruebas)<br>
  <a href="http://www.einbilder.de">Daniel Noll</a> (imágenes)
<h4>Traductores</h4>
  <a href="https://gitlab.com/eson">Åke Engelbrektson</a> (sv)<br>
  <a href="https://gitlab.com/carmenfdezb">Carmen F. B.</a> (es)<br>
  <a href="https://github.com/lturpinat/">Louis Turpinat</a> (fr)
</p>

<p>
<h3><a href="https://gitlab.com/cy8aer/podqast">PodQast</a> usa</h3>
<a href="https://github.com/kurtmckee/feedparser">feedparser</a>
de Kurt McKee y Mark Pilgrim<br>
<a href="http://gpodder.org/mygpoclient/">mygpoclient</a> de Thomas Perl y otros<br>
<a href="https://github.com/Alir3z4/html2text/">html2text</a> de Aaron Schwartz
y otros<br>
<a href="https://github.com/quodlibet/mutagen">mutagen</a> de Christoph Reiter y Joe Wreschnig
</p>

<p>
<h3>Privacidad</h3>
PodQast almacena toda la información en el dispositivo. Usa el servicio de <a href="https://gpodder.net">gpodder.net</a> para consultar la información de los podcasts. Cada suscripción podría manejar datos del usuario. Por favor, infórmate de esos servicios.
</p>

<p>
<small>Version 2.6</small>
</p>

<p>Copyright (c) 2018 Thomas Renard</p>
