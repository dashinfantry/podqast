"""
Archive of listened poposts
"""

import sys
import time

sys.path.append("../")

from podcast.singleton import Singleton
from podcast.factory import Factory

archivename = "the_archive"


class Archive:
    """
    The podcast Archive. It has a list of podposts
    """

    def __init__(self):
        """
        Initialization
        """

        self.podposts = []  # Id list of posposts

    def save(self):
        """
        pickle this element
        """

        store = Factory().get_store()
        store.store(archivename, self)

    def insert(self, podpost):
        """
        Insert an podost
        """

        if podpost not in self.podposts:
            self.podposts.insert(0, podpost)
            post = Factory().get_podpost(podpost)
            if post.insert_date == None:
                post.insert_date = time.time()
            post.save()
            self.save()

    def get_podposts(self):
        """
        get the list of podposts
        """

        for podpost in self.podposts:
            yield podpost

    def remove_podpost(self, podpost):
        """
        Remove a podpost from archive
        """

        self.podposts.remove(podpost)


class ArchiveFactory(metaclass=Singleton):
    """
    Factory  which creates an Archive if it does not exist or gets the pickle
    otherwise if returns the Singleton
    """

    def __init__(self, progname=("harbour-podqast")):
        """
        Initialization
        """

        self.archive = None

    def get_archive(self):
        """
        Get the Archive
        """

        if not self.archive:
            self.archive = Factory().get_store().get(archivename)
            if not self.archive:
                self.archive = Archive()

        return self.archive
