#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pyotherside
import threading
import sys
import os
import hashlib

sys.path.append("/usr/share/harbour-podqast/python")

from podcast.external import ExternalFactory
from podcast.inbox import InboxFactory
from podcast.archive import ArchiveFactory
from podcast.queue import QueueFactory

from podcast.factory import Factory
from podcast.podpost import Podpost
import inotify.adapters
import mutagen


def movePost(postid, moveto):
    """
    Do move the object into the right place
    Returns page for Notification
    """

    if moveto == 0:
        InboxFactory().get_inbox().insert(postid)
    elif moveto == 1:
        QueueFactory().get_queue().insert_next(postid)
    elif moveto == 2:
        QueueFactory().get_queue().insert_bottom(postid)
    elif moveto == 3:
        ArchiveFactory().get_archive().insert(postid)


def check_new():
    """
    check new files in external_home
    """

    pyotherside.send("Check new/lost audio files")
    external = Factory().external_home
    exlib = ExternalFactory().get_external()
    mfiles = []
    for f in os.listdir(external):
        pyotherside.send("file: " + f)
        fn = os.path.join(external, f)
        try:
            if os.path.isfile(fn):
                if mutagen.File(fn):
                    pyotherside.send("have an audio file " + fn)
                    fnh = hashlib.sha256(
                        os.path.join(external, f).encode()
                    ).hexdigest()
                    mfiles.append(fnh)
                    if fnh not in exlib.podposts:
                        pyotherside.send("external: " + fn + " is music")
                        post = Podpost.from_audio_file(fn)
                        post.save()
                        exlib.insert(post.id)
                        exlib.save()
                        wheremove = Factory().get_val("extMoveToVal")
                        movePost(post.id, wheremove)
                        pyotherside.send(
                            "updatesNotification",
                            post.title,
                            post.title,
                            "External",
                        )
                        pyotherside.send("externalUpdated")
        except:
            pyotherside.send("File does not exist")

    for f in exlib.podposts:
        pyotherside.send("we are on file %s" % f)
        if f not in mfiles:
            exlib.remove_podpost(f)
            pyotherside.send("externalUpdated")


def wait_new():
    """
    check via inotify for new files in ~/podqast/external
    """

    external = Factory().external_home
    i = inotify.adapters.Inotify()
    i.add_watch(external)

    def audio_podpost(filenpath):
        try:
            if mutagen.File(filepath):
                pyotherside.send("external is music")
                post = Podpost.from_audio_file(filepath)
                pyotherside.send(post.title)
                pyotherside.send(post.logo_url)
                pyotherside.send(post.id)
                post.save()
                exlib = ExternalFactory().get_external()
                exlib.insert(post.id)
                exlib.save()
                movePost(post.id, wheremove)
                pyotherside.send(
                    "updatesNotification", post.title, post.title, "External"
                )
                pyotherside.send("externalUpdated")
        except:
            pyotherside.send("File does not exist")

    fileopened = {}

    while not stop:
        for event in i.event_gen(timeout_s=10, yield_nones=False):
            (_, type_names, path, filename) = event

            wheremove = Factory().get_val("extMoveToVal")
            if "IN_CREATE" in type_names:
                filepath = os.path.join(external, filename)
                if os.path.islink(filepath):
                    pyotherside.send("Created by ln")
                    audio_podpost(filepath)
                else:
                    fileopened[filename] = True

            if "IN_MODIFY" in type_names:
                filepath = os.path.join(external, filename)
                if filepath in fileopened:
                    fileopened.pop(filepath)

            if "IN_CLOSE_WRITE" in type_names and filename in fileopened:
                filepath = os.path.join(external, filename)
                if filepath in fileopened:
                    pyotherside.send("Createdby cp")
                    audio_podpost(filepath)
                    fileopened.pop(filepath)

            if "IN_MOVED_TO" in type_names:
                filepath = os.path.join(external, filename)
                pyotherside.send("Created by mv")
                audio_podpost(filepath)

            if "IN_DELETE" in type_names:
                podid = hashlib.sha256(
                    os.path.join(external, filename).encode()
                ).hexdigest()
                exlib = ExternalFactory().get_external()
                exlib.remove_podpost(podid)
                exlib.save()
                pyotherside.send("externalUpdated")


def get_external_posts():
    """
    Return a list of all external audio posts
    """

    pyotherside.send("py external_posts")
    entries = []
    external = ExternalFactory().get_external()

    for post in external.get_podposts():
        pyotherside.send("podposts loop")
        entry = Factory().get_podpost(post)
        entries.append(entry.get_data())
    pyotherside.send("after py external")
    # entries.sort(key = lambda r: r["edate"], reverse=True)
    pyotherside.send("createExternalList", entries)


def get_external_pod_data():
    """
    """

    entries = {}

    external = ExternalFactory().get_external()

    for post in external.get_podposts():
        entry = Factory().get_podpost(post)
        if entry.podurl in entries:
            entries[entry.podurl]["count"] += 1
        else:
            if entry.logo_url:
                logo_url = entry.logo_url
            else:
                logo_url = "../../images/podcast.png"
            entries[entry.podurl] = {"count": 1, "logo_url": logo_url}

    pyotherside.send("archivePodList", entries)


def get_audio_data(afile):
    """
    Get info string from mutagen
    """

    try:
        af = mutagen.File(afile)
        pyotherside.send("audioInfo", af.info.pprint())
    except:
        return ""

    return ""


class ExternalHandler:
    def __init__(self):
        self.bgthread = threading.Thread()
        self.bgthread.start()
        self.bgthread2 = threading.Thread()
        self.bgthread2.start()
        self.inothread = threading.Thread()
        self.inothread.start()

        pyotherside.atexit(self.doexit)

    def doexit(self):
        """
        On exit: we need to save ourself
        """

        ExternalFactory().get_external().save()

    def getexternalposts(self):
        if self.bgthread2.is_alive():
            return
        self.bgthread2 = threading.Thread(target=get_external_posts)
        self.bgthread2.start()

    def getexternalpoddata(self):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=get_external_pod_data)
        self.bgthread.start()

    def waitnew(self):
        if self.inothread.is_alive():
            return
        self.inothread = threading.Thread(target=wait_new)
        self.inothread.start()


stop = False
externalhandler = ExternalHandler()
