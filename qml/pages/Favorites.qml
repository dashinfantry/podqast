import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    Component.onCompleted: {
        favoritehandler.getFavorites("home")
    }

    Connections {
        target: favoritehandler
        ignoreUnknownSignals : true
        onFavoriteList: {
            console.log("We are on favorite list")
            archivePostModel.clear()
            for (var i = 0; i < favorites.length; i++) {
                archivePostModel.append(favorites[i]);
            }
        }
    }

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent

        VerticalScrollDecorator { }

        AppMenu { thispage: "Archive" }
        PrefAboutMenu {}

        // Tell SilicaFlickable the height of its content.
        contentHeight: page.height

        Column {
            id: archivetitle

            width: page.width

            spacing: Theme.paddingLarge
            PageHeader {
                title: qsTr("Favorites")
            }
        }

//        PodSelectGrid {
//            id: podselectgrid
//            homeicon: "image://theme/icon-m-favorite-selected"
//            anchors.top: archivetitle.bottom
//            height: Theme.iconSizeLarge
//            z: 3
//            filter: podqast.ffilter
//            Component.onCompleted: {
//                favoritehandler.getFavPodData()
//            }
//            onFilterChanged: {
//                podqast.ffilter = filter
//                archivePostModel.clear()
//                favoritehandler.getFavPodData()
//                favoritehandler.getFavorites(podqast.ffilter)
//            }
//        }

        SilicaListView {
            id: archivepostlist
            anchors.top: archivetitle.bottom
            // anchors.top: podselectgrid.bottom
            width: parent.width
            height: page.height - pdp.height - archivetitle.height // - podselectgrid.height
            section.property: 'asection'
            section.delegate: SectionHeader {
                text: section
                horizontalAlignment: Text.AlignRight
            }

            model: ListModel {
                id: archivePostModel
            }
            delegate: ArchivePostListItem { }
        }

        PlayDockedPanel { id: pdp }
    }
}
