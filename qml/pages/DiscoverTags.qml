import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4
import "../components"

Page {
    id: page
    property int margin: 16
    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    Component.onCompleted: {
        gpodderhandler.getTags()
    }

    Connections {
        target: gpodderhandler
        onTopTags: {
            tagslistModel.clear();
            for (var i=0; i < tagslist.length; i++) {
                console.log(JSON.stringify(tagslist[i]));
                tagslistModel.append(tagslist[i]);
            }
        }
    }

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent
        id: theflick
        AppMenu { thispage: "Discover" }
        PrefAboutMenu {}

        Row {
            id: discovertitle
            width: page.width
            Column {
                id: column
                width: page.width
                spacing: Theme.paddingLarge

                PageHeader {
                    title: qsTr("Discover Tags")
                }
            }
        }

        SilicaListView {
            id: thetagslist
            anchors.top: discovertitle.bottom
            width: parent.width
            height: page.height - pdp.height - discovertitle.height

            model: ListModel {
                id: tagslistModel
                function update() {
                    clear()
                }
            }

            delegate: TagListItem { }
        }

        PlayDockedPanel { id:pdp }
    }
}
