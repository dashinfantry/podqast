import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Thumbnailer 1.0

ListItem {

    property bool isfavorite: favorite
    width: ListView.view.width
    contentHeight: Theme.itemSizeExtraLarge * 1.1

    // onClicked: openMenu()
    onClicked: pageStack.push(Qt.resolvedUrl("../pages/PostDescription.qml"), { title: title, detail: detail,
                              length: length, date: fdate, duration: duration, href: link })

    onPressAndHold: console.log("press and hold")

    clip: true

    Component.onCompleted: {
        if(moved) {
            openMenu()
        }
    }

    menu: Component {
        id: menuComponent
        IconContextMenu {
            id: queueContextMenu

            IconMenuItem {
                text: 'Play'
                icon.source: "image://theme/icon-m-" + (id === playerHandler.firstid && playerHandler.isPlaying ? "pause" : "play")
                onClicked: {
                    if (id === playerHandler.firstid) {
                        playerHandler.playpause()
                    } else {
                        queuehandler.queueInsertTop(id)
                        queuehandler.getQueueEntries()
                    }
                    closeMenu()
                }
            }
            IconMenuItem {
                text: 'Archive'
                icon.source: 'image://theme/icon-m-backup'
                onClicked: {
                    queuehandler.queueToArchive(id)
                    closeMenu()
                    queuehandler.getQueueEntries()
                }
            }
            IconMenuItem {
                id: favoriteicon
                text: qsTr("Favorite")
                icon.source: 'image://theme/icon-m-favorite' + (isfavorite ? "-selected" : "")
                onClicked: {
                    favoritehandler.toggleFavorite(id)
                }
                Connections {
                    target: favoritehandler
                    onFavoriteToggled: {
                        if (id == podpost) {
                            isfavorite = favstate
                        }
                    }
                }
            }
            IconMenuItem {
                id: goup
                text: qsTr("Move up")
                icon.source: 'image://theme/icon-m-back'
                onClicked: {
                    queuehandler.queueMoveUp(id)
                }
            }
            IconMenuItem {
                id: godown
                text: qsTr("Move down")
                icon.source: 'image://theme/icon-m-forward'
                onClicked: {
                    queuehandler.queueMoveDown(id)
                }
            }
        }
    }

    Rectangle {
        id: charrect
        anchors.fill: listicon
        visible: logo_url == ""
        color: Theme.rgba(Theme.highlightColor, 0.5)

        clip: true

        Label {
            anchors.centerIn: parent

            font.pixelSize: parent.height * 0.8
            text: title[0]
            color: Theme.highlightColor
        }
    }


    Thumbnail {
        id: listicon
        property int percentage: dlperc

        anchors.leftMargin: Theme.paddingMedium
        anchors.left: parent.left
        source: logo_url == "" ? "../../images/podcast.png" : logo_url
        width: Theme.iconSizeLarge
        height: Theme.iconSizeLarge
        sourceSize.width: Theme.iconSizeLarge
        sourceSize.height: Theme.iconSizeLarge
        MouseArea {
            anchors.fill: parent
            onClicked: {
                 pageStack.push(Qt.resolvedUrl("../pages/PodpostList.qml"), { url: url })
            }
        }

        Rectangle {
            id: dlstatus
            visible: listicon.percentage > 0 && listicon.percentage < 100
            anchors.right: parent.right
            height: parent.height
            width: (100 - listicon.percentage) * parent.width / 100
            color: "black"
            opacity: 0.7
            z: 0
        }
        Connections {
            target: queuehandler
            onDownloading: {
                if (dlid == id) {
                    listicon.percentage = percent
                }
            }
        }
        Image {
            source: "image://theme/icon-lock-application-update"
            visible: listicon.percentage == 100
            z: 1
            width: parent.width / 4
            height: parent.height / 4
            anchors.bottom: parent.bottom
            anchors.right: parent.right
        }
        Image {
            source: loaded ? "../../images/audio-l.png" : "../../images/audio.png"
            visible: isaudio
            z: 1
            width: parent.width / 4
            height: parent.height / 4
            anchors.bottom: parent.bottom
            anchors.right: parent.right
        }
        Image {
            source: "image://theme/icon-lock-voicemail"
            visible: id === playerHandler.firstid
            z: 1
            width: parent.width / 4
            height: parent.height / 4
            anchors.top: parent.top
            anchors.left: parent.left
        }
    }

    Column {
        anchors.left: listicon.right
        anchors.right: listarrow.left
        anchors.margins: 10
        height: parent.height

        Label {
            id: queueTitlelabel
            width: parent.width
            text: title
            font.pixelSize: Theme.fontSizeExtraSmall
            font.bold: true
            wrapMode: Text.WordWrap
            padding: 5
        }
        Label {
            id: queueTimelabel
            width: parent.width
            font.pixelSize: Theme.fontSizeExtraSmall
            text: timestring
            padding: 5
            Connections {
                target: queuehandler
                onHrtime: {
                    if (dlid == id) {
                        queueTimelabel.text = tmstring
                    }
                }
            }
        }
    }

    IconButton {
        id: listarrow
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        icon.source: "image://theme/icon-m-right"
        onClicked: {
            pageStack.push(Qt.resolvedUrl("../pages/PostDescription.qml"), { title: title, detail: detail,
                               length: length, date: fdate, duration: duration, href: link })
        }
    }
}
