"""
Factory test
"""

import sys
sys.path.append("../python")

from podcast.factory import Factory
from podcast.queue import Queue

import feedparser

def test_singleton():
    """
    Factory is a Singleton. Let's test, if it really is...
    """

    f1 = Factory()             # first instance: set storedir
    f2 = Factory()

    assert f1 == f2

def test_getfeed():
    """
    get a feed
    """

    f = Factory()
    feed1 = f.get_feed("https://freakshow.fm/feed/mp3")
    feed2 = f.get_feed("https://freakshow.fm/feed/mp3")
    feed3 = f.get_feed("https://cre.fm/feed/opus")

    assert feed1 == feed2
    assert type(feed1) == feedparser.FeedParserDict
    
def test_getfeed2():
    """
    get a feed (should work offline)
    """

    f = Factory()
    feed1 = f.get_feed("https://freakshow.fm/feed/mp3")
    feed2 = f.get_feed("https://freakshow.fm/feed/mp3")
    feed3 = f.get_feed("https://cre.fm/feed/opus")
    
    assert feed1 == feed2
    assert feed1 != feed3
    
    assert type(feed1) == feedparser.FeedParserDict
    assert type(feed3) == feedparser.FeedParserDict
    
def test_feed_persist():
    """
    Test feed persisting
    """

    f = Factory()
    f.safe_feed("https://freakshow.fm/feed/mp3")
    f.safe_feed("https://cre.fm/feed/opus")

def test_feed_unpersist():
    """
    test feed removing
    """

    f = Factory()
    f.delete_feed("https://cre.fm/feed/opus")

def test_feed_unpersist2():
    """
    delete other feed too
    """

    f = Factory()
    f.delete_feed("https://freakshow.fm/feed/mp3")
