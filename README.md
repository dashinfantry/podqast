# PodQast
A queue based podcast player for SailfishOS

## Author
Thomas Renard [podqast@g3la.de](mailto:podqast@g3la.de)

### Translations

- Åke Engelbrektson [@eson](https://gitlab.com/eson) (sv)
- Carmen F. B. [@carmenfdezb](https://gitlab.com/carmenfdezb) (es)

## License
Licensed under GNU GPLv3

## Credits

- [BluesLee](https://talk.maemo.org/member.php?u=3116) (testing)
- [Daniel Noll](http://www.einbilder.de) (logo design)

This project uses
- [feedparser](https://github.com/kurtmckee/feedparser) by Kurt McKee and Mark Pilgrim
- [mygpoclient](http://gpodder.org/mygpoclient/) by Thomas Perl and others
- [html2text](https://github.com/Alir3z4/html2text) by Aaron Schwartz and others
- [mutagen](https://github.com/quodlibet/mutagen) by Christoph Reiter and Joe Wreschnig
