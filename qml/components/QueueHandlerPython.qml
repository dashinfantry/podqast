import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4

Python {
    id: queuehandler

    signal createArchiveList(var data)
    signal firstEntry(var data)
    signal setFirst(var data, var chapterlist)
    signal doPlay()
    signal posPlay(var position)
    signal setPos(var position)
    signal doStop()
    signal createList(var data, string moved)
    signal downloading(string dlid, int percent)
    signal hrtime(string dlid, string tmstring)
    signal needDownload(string podpost)
    signal firstDownloaded()

    Component.onCompleted: {
        setHandler("createArchiveList", createArchiveList)
        setHandler("firstentry", firstEntry)
        setHandler("setFirst", setFirst)
        setHandler("doplay", doPlay)
        setHandler("posplay", posPlay)
        setHandler("setpos", setPos)
        setHandler("stopped", doStop)
        setHandler("createList", createList)
        setHandler("downloading", downloading)
        setHandler("hrtime", hrtime)
        setHandler("needDownload", needDownload)
        setHandler("firstDownloaded", firstDownloaded)

        addImportPath(Qt.resolvedUrl('.'));
        importModule('QueueHandler', function () {
            console.log('QueueHandler is now imported')
        })
    }
    function queueInsertTop(id) {
        console.log("queueInsertTop")
        call("QueueHandler.queuehandler.queueinserttop", [id], function() {});
        // call("QueueHandler.queue_insert_top", [id], function() {});
    }
    function queueInsertNext(id) {
        call("QueueHandler.queuehandler.queueinsertnext", [id], function() {});
        // call("QueueHandler.queue_insert_next", [id], function() {});
    }
    function queueInsertBottom(id) {
        call("QueueHandler.queuehandler.queueinsertbottom", [id], function() {});
        // call("QueueHandler.queue_insert_bottom", [id], function() {});
    }
    function queueMoveUp(id) {
        call("QueueHandler.queuehandler.queuemoveup", [id], function() {});
        // call("QueueHandler.queue_move_up", [id], function() {})
    }

    function queueMoveDown(id) {
        call("QueueHandler.queuehandler.queuemovedown", [id], function() {});
        // call("QueueHandler.queue_move_down", [id], function() {})
    }

    function queueToArchive(id) {
        call("QueueHandler.queuehandler.queuetoarchive", [id], function() {});
        // call("QueueHandler.queue_to_archive", [id], function() {});
    }
    function queueTopToArchive() {
        call("QueueHandler.queuehandler.queuetoptoarchive", function() {});
        // call("QueueHandler.queue_top_to_archive", function() {});
    }

    function getQueueEntries() {
        // call("QueueHandler.queuehandler.getqueueposts", function() {});
        call("QueueHandler.get_queue_posts", function() {});
    }
    function getFirstEntry() {
        call("QueueHandler.queuehandler.getfirstentry", function() {});
        // call("QueueHandler.get_first_entry", function() {});
    }
    function queueHrTime() {
        call("QueueHandler.queuehandler.queuehrtime", [mediaplayer.position], function() {});
        // call("QueueHandler.queue_hr_time", [mediaplayer.position], function() {});
    }
    function downloadAudio(podpost) {
        call("QueueHandler.queuehandler.queuedownload", [podpost], function() {});
        // call("QueueHandler.queue_do_download", [podpost], function() {});

    }
    function downloadAudioAll() {
        // doesnotexist?? call("QueueHandler.queuehandler.downloadaudioall", function() {});
        call("QueueHandler.queue_download_all", function() {});
    }

    onError: {
        console.log('python error: ' + traceback);
    }

    onReceived: {
        console.log('got message from python: ' + data);
    }
    onDoStop: {
        console.log("stopped")
        playerHandler.stop()
    }
    onDoPlay: {
        console.log("play")
        playerHandler.play()
    }
    onPosPlay: {
        console.log("posplay")
        playerHandler.seekPos = position
        playerHandler.play()
    }
    onSetPos: {
        console.log("setPos")
        playerHandler.seekPos = position
    }
    onFirstDownloaded: {
        console.log("first Downloaded")
        console.log("Mediaplayer state" + mediaplayer.playbackState)
        if (mediaplayer.playbackState == 1) {
            console.log("we are the first element")
            // playerHandler.pause()
            // playerHandler.play()
        }
    }
}
