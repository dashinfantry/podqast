"""
External Audio data list
"""

import sys
import time
import pyotherside

sys.path.append("../")

from podcast.singleton import Singleton
from podcast.factory import Factory

externalname = "the_external"


class External:
    """
    The  External audio list. It has a list of podposts
    """

    def __init__(self):
        """
        Initialization
        """

        self.podposts = []  # Id list of posposts

    def save(self):
        """
        pickle this element
        """

        store = Factory().get_store()
        store.store(externalname, self)

    def insert(self, podpost):
        """
        Insert an podost
        """

        if podpost not in self.podposts:
            self.podposts.insert(0, podpost)
            post = Factory().get_podpost(podpost)
            if post.insert_date == None:
                post.insert_date = time.time()
            post.save()
            self.save()
        pyotherside.send("got %d entries" % len(self.podposts))

    def get_podposts(self):
        """
        get the list of podposts
        """

        pyotherside.send("rendering %d podposts" % len(self.podposts))

        for podpost in self.podposts:
            yield podpost

    def remove_podpost(self, podpost):
        """
        Remove a podpost from archive
        """

        if podpost in self.podposts:
            self.podposts.remove(podpost)
            self.save()


class ExternalFactory(metaclass=Singleton):
    """
    Factory  which creates an Archive if it does not exist or gets the pickle
    otherwise if returns the Singleton
    """

    def __init__(self, progname=("harbour-podqast")):
        """
        Initialization
        """

        self.external = None

    def get_external(self):
        """
        Get the Archive
        """

        if not self.external:
            self.external = Factory().get_store().get(externalname)
            if not self.external:
                self.external = External()

        return self.external
