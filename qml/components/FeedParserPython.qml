import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4

Python {
    id: feedparserhandler

    signal feedInfo(var pcdata)
    signal firstEvent(var pcdata)
    signal lastEvent(var pcdata)
    signal alternatives(var pcdata)
    signal podcastsList(var pcdata)
    signal createPodpostsList(var pcdata, string podtitle, string podlink)
    signal podcastParams(var pcdata)
    signal subscribed(string pcurl)
    signal updatesNotification(string pctitle, string pstitle, string page)
    signal newPodcasts(var posts)
    signal htmlfile(string htmlfile)
    signal refreshProgress(real progress)
    signal refreshPost(string posttitle)
    signal opmlImported(int opmlcount)
    signal appError(string errmessage)
    signal backupDone(string tarpath)
    signal opmlSaveDone(string opmlpath)

    Component.onCompleted: {
        setHandler("feedinfo", feedInfo)
        setHandler("fevent", firstEvent)
        setHandler("levent", lastEvent)
        setHandler("alternatives", alternatives)
        setHandler("podcastslist", podcastsList)
        setHandler("createPodpostsList", createPodpostsList)
        setHandler("podcastParams", podcastParams)
        setHandler("subscribed", subscribed)
        setHandler("updatesNotification", updatesNotification)
        setHandler("newPodcasts", newPodcasts)
        setHandler("htmlfile", htmlfile)
        setHandler("refreshProgress", refreshProgress)
        setHandler("refreshPost", refreshPost)
        setHandler("opmlimported", opmlImported)
        setHandler("apperror", appError)
        setHandler("backupDone", backupDone)
        setHandler("opmlSaveDone", opmlSaveDone)

        addImportPath(Qt.resolvedUrl('.'));
        importModule('FeedParser', function () {
            console.log('FeedParser is now imported')
            call("FeedParser.get_object", [podqast], function() {});
        });
    }
    function getPodcast(url) {
        console.log("url: " + url)
        call("FeedParser.feedparse.getfeedinfo", [url], function() {});
        // call("FeedParser.get_feedinfo", [url], function() {});
    }
    function getPodcasts() {
        call("FeedParser.feedparse.getpodcasts", function() {});
        // call("FeedParser.get_podcasts", function() {});
    }
    function getPodcastsPreCache() {
        call("FeedParser.feedparse.getpodcastscache", function() {});
        // call("FeedParser.get_podcastscache", function() {});
    }

    function getEntries(url) {
        console.log("url: " + url)
        call("FeedParser.feedparse.getentries", [url], function() {});
        // call("FeedParser.get_entries", [url], function() {});
    }
    function getFirstEvent(url) {
        console.log("getFirstElement")
        call("FeedParser.feedparse.getfirstentry", [url], function() {});
        // call("FeedParser.get_first_entry", [url], function() {});

    }
    function getLastEvent(url) {
        console.log("getLastElement")
        console.log(url)
        call("FeedParser.feedparse.getlastentry", [url], function() {});
        // call("FeedParser.get_last_entry", [url], function() {});
    }

    function getAlternatives(url) {
        call("FeedParser.feedparse.getalternatives", [url], function() {})
        // call("FeedParser.get_alternatives", [url], function() {})
    }

    function subscribePodcast(url) {
        console.log("Subscribe url: " + url)
        call("FeedParser.feedparse.subscribepodcast", [url], function() {})
        // call("FeedParser.subscribe_podcast", [url], function() {})
    }
    function subscribePodcastFg(url) {
        console.log("Subscribe url: " + url)
        // Needs foreground! call("FeedParser.feedparse.subscribepodcast", [url], function() {})
        call("FeedParser.subscribe_podcast", [url], function() {})
    }
    function deletePodcast(url) {
        console.log("Delete url: " + url)
        call("FeedParser.feedparse.deletepodcast", [url], function() {})
        // call("FeedParser.delete_podcast", [url], function() {})
    }
    function refreshPodcast(url) {
        console.log("Refresh url: " + url)
        _showUpdatesNotification = true
        //call("FeedParser.feedparse.refreshpodcast", [url, moveToConf.value,
        //     doDownloadConf.value && (wifiConnected || doMobileDownConf.value), autoLimitConf.value], function() {})
        call("FeedParser.refresh_podcast", [url, moveToConf.value,
             doDownloadConf.value && (wifiConnected || doMobileDownConf.value), autoLimitConf.value], function() {})
    }
    function refreshPodcasts() {
        console.log("Refreshing all podcasts.")
        _showUpdatesNotification = true
        call("FeedParser.feedparse.refreshpodcasts", [moveToConf.value,
              doDownloadConf.value && (wifiConnected || doMobileDownConf.value), autoLimitConf.value], function() {})
        // call("FeedParser.refresh_podcasts", [moveToConf.value,
        //  doDownloadConf.value && (wifiConnected || doMobileDownConf.value), autoLimitConf.value], function() {})
        var d = new Date()
        var seconds = Math.round(d.getTime() / 1000)
        lastRefreshed.value = seconds
    }

    function getPodcastParams(url) {
        console.log("Get Podcast params")
        call("FeedParser.feedparse.getpodcastparams", [url], function() {})
        // call("FeedParser.get_podcast_params", [url], function() {})
    }
    function setPodcastParams(url, params) {
        console.log("Set Podcast Params")
        call("FeedParser.feedparse.setpodcastparams", [url, params], function() {})
        // call("FeedParser.set_podcast_params", [url, params], function() {})
    }

    function renderHtml(data) {
        console.log("Set Podcast Params")
        call("FeedParser.feedparse.renderhtml", [data], function() {})
        // call("FeedParser.render_html", [data], function() {})
    }

    function nomedia(doset) {
        console.log("Set Podcast Params")
        // call("FeedParser.feedparse.nomedia", [doset], function() {})
        call("FeedParser.nomedia", [doset], function() {})
    }
    function importOpml(opmlfile) {
        console.log("Import Podcasts from OPML")
        // call("FeedParser.feedparse.importopml", [opmlfile], function() {})
        call("FeedParser.import_opml", [opmlfile], function() {})
    }
    function importGpodder() {
        console.log("Import Podcasts from Gpodder database")
        // call("FeedParser.feedparse.importgpodder", function() {})
        call("FeedParser.import_gpodder", function() {})
    }
    function doBackup() {
        console.log("Backup")
        // call("FeedParser.feedparse.dobackup", function() {})
        call("FeedParser.do_backup", function() {})
    }
    function doWriteOpml() {
        console.log("Writing opml file")
        // call("FeedParser.feedparse.writeopml", function() {})
        call("FeedParser.write_opml", function() {})
    }
    function moveArchive(id) {
        // call("FeedParser.feedparse.movearchive", [id], function() {})
        call("FeedParser.move_archive", [id], function() {})
    }


    onError: {
        console.log('python error: ' + traceback);
    }

    onReceived: {
        console.log('got message from python: ' + data);
    }
    onAppError: {
        console.log("Notify error " + errmessage)
        appNotification.previewSummary = qsTr("Error")
        appNotification.previewBody = errmessage
        appNotification.body = errmessage
        appNotification.publish()
    }
}
