"""
factory for podcast. This factory glues all the cache persistant and new element stuff
for all used podcast elements.

Factory is a Singleton
"""

import sys
import os

sys.path.append("../")

from podcast.singleton import Singleton
from podcast.cache import Cache
from podcast.store import Store

from podcast import util

import feedparser
import pyotherside


class Factory(metaclass=Singleton):
    """
    Factory for
    """

    def __init__(self, progname="harbour-podqast"):
        """
        Initialization
        feedcache: cache for feeds
        store: store for feeds
        """

        self.queue = None
        self.feedcache = Cache()
        self.object = None

        home = os.path.expanduser("~")
        xdg_data_home = os.environ.get(
            "XDG_DATA_HOME", os.path.join(home, ".local", "share")
        )
        xdg_config_home = os.environ.get(
            "XDG_CONFIG_HOME", os.path.join(home, ".config")
        )
        xdg_cache_home = os.path.join(
            "XDG_CACHE_HOME", os.path.join(home, ".cache")
        )
        self.data_home = os.path.join(xdg_data_home, progname)
        self.config_home = os.path.join(xdg_config_home, progname)
        self.cache_home = os.path.join(xdg_cache_home, progname)

        if "PODQAST_HOME" in os.environ:
            home = os.environ["PODQAST_HOME"]
            self.data_home = self.config_home = self.cache_home = home

        storepath = os.path.join(self.data_home, "store")
        self.iconpath = os.path.join(self.data_home, "icons")
        self.afilepath = os.path.join(self.data_home, "audio")
        util.make_directory(self.data_home)
        util.make_directory(self.config_home)
        util.make_directory(storepath)
        util.make_directory(self.iconpath)
        util.make_directory(self.afilepath)

        self.store = Store(storepath)
        self.podpostcache = Cache(limit=500)

    def get_object(self, object):
        """
        Get QML object
        """

        try:
            if self.object:
                return
        except:
            pass

        self.object = object

        home = os.path.expanduser("~")

        self.media_home = os.path.join(self.get_val("musicHomeVal"), "podqast")
        self.favorites_home = os.path.join(self.media_home, "favorites")
        self.external_home = os.path.join(self.media_home, "external")
        xdg_media_home = os.path.join(
            "XDG_DATA_DIRS", os.path.join(home, "podqast")
        )
        if os.path.exists(xdg_media_home):
            os.rename(xdg_media_home, self.media_home)

        if self.get_val("allowExtVal") or self.get_val("allowFavVal"):
            util.make_directory(self.media_home)
            util.make_directory(self.favorites_home)
            util.make_directory(self.external_home)
            epath = os.path.join(self.external_home, ".nomedia")
            open(epath, "a").close()

        pyotherside.send("objectLoaded")

    def get_val(self, valname):
        """
        Return property value by valname
        """

        try:
            return getattr(self.object, valname)
        except:
            return None

    def get_feed(self, index):
        """
        get a feed object by index
        index: index of feed
        """

        feed = self.feedcache.get(index)
        if not feed:
            feed = self.store.get(index)
            if not feed:
                feed = feedparser.parse(index)
                if feed.bozo != 0:
                    pyotherside.send("get_feed: error in feed parsing")
                    return None
            self.feedcache.store(index, feed)

        return feed

    def safe_feed(self, index):
        """
        Safe feed to disk
        """

        feed = self.get_feed(index)
        if feed:
            self.store.store(index, feed)

    def delete_feed(self, index):
        """
        Delete file from disk
        """

        self.store.delete(index)

    def get_podpost(self, index):
        """
        Get a podpost from cache or store
        """

        podpost = self.podpostcache.get(index)
        if not podpost:
            podpost = self.store.get(index)
            if podpost:
                self.podpostcache.store(index, podpost)
            else:
                return None

        return podpost

    def get_store(self):
        """
        get the store
        """

        return self.store

    def nomedia(self, doset):
        """
        create .nomedia file in root path of podqast
        """

        audiopath = os.path.join(self.data_home, "audio", ".nomedia")
        iconpath = os.path.join(self.data_home, "icons", ".nomedia")
        open(iconpath, "a").close()

        if doset == False:
            open(audiopath, "a").close()
        else:
            os.remove(audiopath)
