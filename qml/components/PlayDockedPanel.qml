import QtQuick 2.0
import Sailfish.Silica 1.0

DockedPanel {
    id: playdockedpanel

    width: parent.width
    height: Theme.itemSizeSmall
    open: playeropen

    dock: Dock.Bottom

    Rectangle {
        anchors.fill: parent
        color: Theme.highlightDimmerColor
        opacity: 0.7
    }

    Row {
        id: row
        anchors.centerIn: parent

        IconButton {
            icon.source: "image://theme/icon-m-previous"
            onClicked: {
                podqast.fast_backward()
            }
        }
        IconButton {
            icon.source: "image://theme/icon-m-" + (playerHandler.isPlaying ? "pause" : "play")
            onClicked: {
                playerHandler.playpause()
            }
        }
        IconButton {
            icon.source: "image://theme/icon-m-next"
            onClicked: {
                podqast.fast_forward()
            }
        }
        IconButton {
            id: playpanelicon
            icon.source: playerHandler.playicon === "" ? "../../images/podcast.png" : playerHandler.playicon
            icon.width: Theme.iconSizeMedium
            icon.height: Theme.iconSizeMedium
            icon.color: undefined
            onClicked: {
                queuehandler.getFirstEntry()
            }
            Connections {
                target: queuehandler
                onFirstEntry: {
                    pageStack.push(Qt.resolvedUrl("../pages/PostDescription.qml"),
                                   { title: data.title, detail: data.detail,
                                       length: data.length, date: data.fdate, duration: data.duration,
                                       href: data.link
                                   })
                }
            }
        }
        IconButton {
            id: podimage
            icon.source: "image://theme/icon-m-right"
            onClicked: pageStack.push(Qt.resolvedUrl("../pages/Player.qml"))
        }
        Image {
            source: "image://theme/icon-s-timer"
            visible: podqast.dosleep
        }
        Image {
            source: "image://theme/icon-s-duration"
            visible: playerHandler.playrate !== 1.0
        }
    }
}
